package com.hotel;

public class Hotel {
    private String hotelName;
    private PriceCatalog priceCatalog;
    private int ratings;

    public Hotel(String hotelName, PriceCatalog priceCatalog, int ratings) {

        this.hotelName = hotelName;
        this.priceCatalog = priceCatalog;
        this.ratings = ratings;
    }

    public int getPrice(CustomerType customerType, int noOfWeekdays, int noOfWeekends) {
        return priceCatalog.getPrice(customerType, noOfWeekdays, noOfWeekends);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hotel hotel = (Hotel) o;

        if (ratings != hotel.ratings) return false;
        if (hotelName != null ? !hotelName.equals(hotel.hotelName) : hotel.hotelName != null) return false;
        return priceCatalog != null ? priceCatalog.equals(hotel.priceCatalog) : hotel.priceCatalog == null;

    }

    @Override
    public int hashCode() {
        int result = hotelName != null ? hotelName.hashCode() : 0;
        result = 31 * result + (priceCatalog != null ? priceCatalog.hashCode() : 0);
        result = 31 * result + ratings;
        return result;
    }

    public Hotel getMostRated(Hotel hotel) {
        if (ratings > hotel.ratings) return this;
        return hotel;
    }
}
