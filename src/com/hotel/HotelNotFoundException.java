package com.hotel;

public class HotelNotFoundException extends Throwable {
    public HotelNotFoundException(String message) {
        super(message);
    }
}
