package com.hotel;

import java.util.ArrayList;
import java.util.List;

public class Hotels {
    List<Hotel> hotels;

    Hotels() {
        hotels = new ArrayList<>();
    }

    public Hotel getCheapestHotel(CustomerType customerType, int noOfWeekends, int noOfWeekdays) throws HotelNotFoundException {
        if (hotels.size() ==0) {
            throw new HotelNotFoundException("No Hotels Are Available");
        }
        int lowestCost = hotels.get(0).getPrice(customerType, noOfWeekdays, noOfWeekends);
        Hotel cheapestHotel = hotels.get(0);
        for (Hotel hotel : hotels) {
            int price = hotel.getPrice(customerType, noOfWeekdays, noOfWeekends);
            if(price==lowestCost){
                cheapestHotel=cheapestHotel.getMostRated(hotel);
            }
            if (price < lowestCost) {
                lowestCost = price;
                cheapestHotel = hotel;
            }
        }
        return cheapestHotel;

    }

    public void addHotel(Hotel hotel) {
        hotels.add(hotel);
    }
}
