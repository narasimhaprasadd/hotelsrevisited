package com.hotel;

public class PriceCatalog {
    private int weekdayNormal;
    private int weekendNormal;
    private int weekdayReward;
    private int weekendReward;

    public PriceCatalog(int weekdayNormal, int weekendNormal, int weekdayReward, int weekendReward) {

        this.weekdayNormal = weekdayNormal;
        this.weekendNormal = weekendNormal;
        this.weekdayReward = weekdayReward;
        this.weekendReward = weekendReward;
    }

    public int getPrice(CustomerType customerType, int noOfWeekdays, int noOfWeekends) {
        if (customerType.equals("regular"))
            return weekdayNormal*noOfWeekdays + weekendNormal*noOfWeekends;
        return weekdayReward*noOfWeekdays + weekendReward*noOfWeekends;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceCatalog that = (PriceCatalog) o;

        if (weekdayNormal != that.weekdayNormal) return false;
        if (weekendNormal != that.weekendNormal) return false;
        if (weekdayReward != that.weekdayReward) return false;
        return weekendReward == that.weekendReward;

    }

    @Override
    public int hashCode() {
        int result = weekdayNormal;
        result = 31 * result + weekendNormal;
        result = 31 * result + weekdayReward;
        result = 31 * result + weekendReward;
        return result;
    }
}
