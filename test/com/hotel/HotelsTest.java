package com.hotel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class HotelsTest {

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1WeekendNormalCustomer() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Shanth Sagar", priceCatalog, 10);

        PriceCatalog price = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel = new Hotel("Shanth Sagar", price, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.regular, 1, 0);

        assertEquals(expectedHotel, actualHotel);
    }

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1Weekend1WeekdayNormalCustomer() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Shanth Sagar", priceCatalog, 10);

        PriceCatalog price = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel = new Hotel("Shanth Sagar", price, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.regular, 1, 1);

        assertEquals(expectedHotel, actualHotel);
    }

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1WeekendRewardCustomer() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Shanth Sagar", priceCatalog, 10);

        PriceCatalog price = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel = new Hotel("Shanth Sagar", price, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.reward, 1, 0);

        assertEquals(expectedHotel, actualHotel);
    }

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1WeekdayRewardCustomer() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Shanth Sagar", priceCatalog, 10);

        PriceCatalog price = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel = new Hotel("Shanth Sagar", price, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.reward, 0, 1);

        assertEquals(expectedHotel, actualHotel);
    }

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1Weekday1WeekendRewardCustomer() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Shanth Sagar", priceCatalog, 10);

        PriceCatalog price = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel = new Hotel("Shanth Sagar", price, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.reward, 1, 1);

        assertEquals(expectedHotel, actualHotel);
    }

    @Rule
    public ExpectedException expectedException=ExpectedException.none();
    @Test
    public void shouldBeAbleToGetMessageWhenThereAreNoHotelsAvailable() throws HotelNotFoundException {
        expectedException.expect(HotelNotFoundException.class);
        expectedException.expectMessage("No Hotels Are Available");
        Hotels hotels = new Hotels();

        hotels.getCheapestHotel(CustomerType.reward, 1, 1);

    }

    @Test
    public void shouldBeAbleToGetCheapestHotelFor1Weekday1WeekendRewardCustomerWhereThereAreTwoHotelsAvailable() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 50, 80);
        Hotel actualHotel = new Hotel("Suvarna Sagar", priceCatalog, 10);

        PriceCatalog price1 = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel1 = new Hotel("Shanth Sagar", price1, 10);
        PriceCatalog price2 = new PriceCatalog(80, 100, 50, 80);
        Hotel hotel2 = new Hotel("Suvarna Sagar", price2, 10);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel1);
        hotels.addHotel(hotel2);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.reward, 1, 1);

        assertEquals(expectedHotel, actualHotel);
    }

    @Test
    public void shouldBeAbleToGetCheapestHotelWithHighestRatingWhenCostOfTwoHotelsAreSame() throws HotelNotFoundException {
        PriceCatalog priceCatalog = new PriceCatalog(80, 100, 70, 80);
        Hotel actualHotel = new Hotel("Suvarna Sagar", priceCatalog, 7);

        PriceCatalog price1 = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel1 = new Hotel("Shanth Sagar", price1, 5);
        PriceCatalog price2 = new PriceCatalog(80, 100, 70, 80);
        Hotel hotel2 = new Hotel("Suvarna Sagar", price2, 7);

        Hotels hotels = new Hotels();
        hotels.addHotel(hotel1);
        hotels.addHotel(hotel2);

        Hotel expectedHotel = hotels.getCheapestHotel(CustomerType.regular, 1, 1);

        assertEquals(expectedHotel, actualHotel);
    }



}
